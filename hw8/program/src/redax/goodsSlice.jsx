
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
  goods: [],
  status: 'idle', 
  error: null,

};

const fetchGoods = createAsyncThunk('goods/fetchGoods', async () => {
  const response = await fetch('../goods.json');
  if (!response.ok) {
    throw new Error('Не вдалося отримати товари.');
  }
  const data = await response.json();
  return data;
});

const goodsSlice = createSlice({
  name: 'goods',
  initialState,
  reducers: {

  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchGoods.pending, (state) => {
        state.status = 'loading'; 
      })
      .addCase(fetchGoods.fulfilled, (state, action) => {
        state.status = 'succeeded'; 
        state.goods = action.payload;
      })
      .addCase(fetchGoods.rejected, (state, action) => {
        state.status = 'failed'; 
        state.error = action.error.message;
      });
  },
});


export default goodsSlice.reducer;
export { fetchGoods };

