import ProductList from "../components/ProductList/ProductList";
import Modal from "../components/Modal/Modal";
import { useSelector } from "react-redux";
import { getStateModal } from "../redax/modalSlice";
import { getBasketCard } from "../redax/basketSlice";
import SignupForm from "../components/Form/Form";
import { useEffect, useState } from "react";
import DataContaxt from "../context/DataContaxt";
import { ViewProvider } from '../context/ViewProvider';
import ViewSwitcher from '../context/ViewSwitcher';

const Basket = () => {

    const { modalOpen, selectedProductId  } = useSelector(getStateModal);
    const arrayBasket = useSelector(getBasketCard);

    const [showDelivery, setShowDelivery] = useState(false);

    useEffect(() => {
        setShowDelivery(arrayBasket.length > 0);
    }, [arrayBasket]);

    const data = arrayBasket.find((item) => item.id === selectedProductId);

    return (
        <ViewProvider>
        <div className={showDelivery ? "main__wrapper" : "main__container"}>
            <div className="flex__element">
                    <h2 className="subtitle__page subtitle__page--center">Товари з корзини</h2>
                    <ViewSwitcher />
                <ProductList data={arrayBasket}/>
            </div>
                {showDelivery ?<SignupForm /> : null }
        </div>
            
        <DataContaxt.Provider value={data}>
                {modalOpen && <Modal  />}
        </DataContaxt.Provider>
        </ViewProvider>
    )
}
export default Basket;