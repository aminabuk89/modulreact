import React, { createContext, useState } from 'react';

const ViewContext = createContext();

const ViewProvider = ({ children }) => {
  const [view, setView] = useState('grid'); 

  return (
    <ViewContext.Provider value={{ view, setView }}>
      {children}
    </ViewContext.Provider>
  );
};

export { ViewContext, ViewProvider };