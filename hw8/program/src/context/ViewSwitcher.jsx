import React, { useContext } from 'react';
import { ViewContext } from '../context/ViewProvider';

const ViewSwitcher = () => {
  const { view, setView } = useContext(ViewContext);

  return (
    <div className="view__switcher">
      <button onClick={() => setView('grid')} className={`btn__toggle ${view === 'grid' ? 'btn__toggle--active' : ''}`}>Картки</button>
      <button onClick={() => setView('list')} className={`btn__toggle ${view === 'list' ? 'btn__toggle--active' : ''}`}>Таблиця</button>
    </div>
  );
};

export default ViewSwitcher;