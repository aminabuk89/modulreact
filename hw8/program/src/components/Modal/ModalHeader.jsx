import { useContext } from "react";
import DataContaxt from "../../context/DataContaxt";

const ModalHeader = () => {

    const data = useContext(DataContaxt)
    
    return (
        <div className="card__container">
            <img className="card__photo card__photo--modal" src={data.img} alt="photo goods" />
            <p className="card__description">
                <span className="card__description card__description--title">Детальніше про товар:  </span>
                {data.description}
            </p>
        </div>
        )
}

export default ModalHeader;

