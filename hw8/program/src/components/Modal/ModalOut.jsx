import { useContext } from "react";
import CloseModalContaxt from "../../context/CloseModalContaxt";

const ModalOut = ({ children}) => {

  const handleCloseModal = useContext(CloseModalContaxt);

    const handleClickOutside = (e) => {
    if (handleCloseModal && e.target.classList.contains('card__modal')) {
      handleCloseModal();
    }
  };

    return (
        <div className="card__modal" onClick={handleClickOutside}>{ children}</div>
    )
}
export default ModalOut;

