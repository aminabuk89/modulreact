import { useLocation  } from "react-router-dom";
import heartBlack from "../../assets/icon-black.png";
import heartPink from "../../assets/icon-pink.png"
import closeBasket from "../../assets/close.png";
import closeFavorite from "../../assets/close-star.png"
import { openModal } from '../../redax/modalSlice'; 
import { useDispatch, useSelector } from "react-redux";
import { getBasketCard } from "../../redax/basketSlice";
import { getFavoriteCard } from "../../redax/favoriteSlice";
import { ViewContext } from '../../context/ViewProvider';
import React, { useContext } from 'react';

const Card = ({ title, img, color, price, article, id }) => {

    const location = useLocation();
    const currentPage = location.pathname;

    const  arrayBasket  = useSelector(getBasketCard);
    const  arrayFavorite  = useSelector(getFavoriteCard);

    const inBasket = arrayBasket.some(item => item.id === id);
    const isFavorite = arrayFavorite.some(item => item.id === id);
    
    const dispatch = useDispatch();
   
    const handleOpenModal = (productId, modalType) => {
        dispatch(openModal({ productId, modalType}));
    };

    const handleBasketAdd = () => {
        handleOpenModal(id, "basket");   
    };

    const handleFavoriteAdd = () => {
        if (isFavorite) {
            handleOpenModal(id, "favoriteClose");
        } else {
            handleOpenModal(id, "favorite");
        } 
    };

    const clickInBasketClose = () => {
        handleOpenModal(id,"basketClose");
    }

    const clickInFavoriteClose = () => {
        handleOpenModal(id,"favoriteClose");
    }

    const { view } = useContext(ViewContext);

if (view !== "grid") {
    return (
    
        <table>
            <tr>
                <td>{title}</td>
                <td>
                    <img className="card__photo" src={img} alt="photo goods" />
                </td>
                <td>{color}</td>
                <td>{price}</td>
                <td>{article}</td>
                {currentPage === "/" &&
                    <>
                        <td>
                            <button className={
                                inBasket
                                    ? "card__button card__button--active card__button--size-small"
                                    : "card__button card__button--posiv card__button--size-small"
                            } onClick={handleBasketAdd} disabled={inBasket}>
                                {inBasket ? "Товар в кошику" : "Додати до кошика"}
                            </button>
                        </td>
                        <td>
                            <button className="star__btn--table star__btn--initial-pos--right" onClick={handleFavoriteAdd} disabled={isFavorite}>
                                <img className="star__initial star__initial--pos-right" src={isFavorite ? heartPink : heartBlack} alt="heart icon" />
                            </button>
                        </td>
                    </>
                }
                {currentPage === "/favorite" &&
                    <>
                        <td>
                            <button className="star__btn--table star__btn--initial-pos--right" onClick={clickInFavoriteClose}>
                                <img src={closeFavorite} className="star__initial star__initial--pos-right" alt="close"/>
                            </button>
                        </td>
                        <td>
                            <div className="btn__conteiner">
                                <button className={inBasket ? "card__button card__button--active card__button--size-half"
                                    : "card__button card__button--posiv card__button--size-half"} onClick={handleBasketAdd} disabled={inBasket}>
                                    {inBasket ? "Товар в кошику" : "Додати до кошика"}
                                </button>
                                {inBasket ? <button className="card__button card__button--size-half" onClick={clickInBasketClose}>Видалити з кошика</button> : null} 
                            </div>
                        </td>
                        
                    </>
                }
                {currentPage === "/basket" &&
                    <>
                        <td>
                            <button className="star__btn--table star__btn--initial-pos--right" onClick={handleFavoriteAdd} disabled={isFavorite}>
                                <img className="star__initial star__initial--pos-right" src={isFavorite ? heartPink : heartBlack} alt="heart icon" />
                            </button>
                        </td>
                        <td>
                            <button className="star__btn--table star__btn--initial-pos--right" onClick={clickInBasketClose}>
                                <img className="star__initial--close star__initial--pos-right" src={closeBasket} alt="close"/>
                            </button>
                        </td>
                        
                    </>
                }
                   
    </tr>
    </table>
  
        
    );
}

if (view === "grid") {
    return (
        <li className="card__item--list">
            <h2 className='card__title card__title--list'>{title}</h2>
            <img className="card__photo" src={img} alt="photo goods" />
            <div className='card__text--list'>
                <p className="card__text--color text">Колір: {color}</p>
                <span className="card__text--price text">Ціна: {price}</span>
                <p className="card__text--number text">Артикул: {article}</p>
            </div>
            {currentPage === "/" &&
                <>
                    <button className={
                        inBasket
                            ? "card__button card__button--active card__button--size-middle"
                            : "card__button card__button--posiv card__button--size-middle"
                    } onClick={handleBasketAdd} disabled={inBasket}>
                        {inBasket ? "Товар в кошику" : "Додати до кошика"}
                    </button>
                    <button className="star__btn--initial star__btn--initial-pos--right" onClick={handleFavoriteAdd} disabled={isFavorite}>
                        <img className="star__initial star__initial--pos-right"  src={isFavorite ? heartPink : heartBlack} />
                    </button>
                </>
            }
            {currentPage === "/favorite" &&
                <>
                    <button className="star__btn--initial star__btn--initial-pos--right" onClick={clickInFavoriteClose}>
                        <img src={closeFavorite} className="star__initial star__initial--pos-right" alt="close"/>
                    </button>
                    <div className="btn__conteiner">
                        <button className={inBasket ? "card__button card__button--active card__button--size-small" : "card__button card__button--posiv card__button--size-large"} onClick={handleBasketAdd} disabled={inBasket}>
                            {inBasket ? "Товар в кошику" : "Додати до кошика"}
                        </button>
                        {inBasket ? <button className="card__button card__button--size-small" onClick={clickInBasketClose}>Видалити з кошика</button> : null} 
                    </div>
                </>
            }
            {currentPage === "/basket" &&
                <>
                    <button className="star__btn--initial star__btn--initial-pos--right" onClick={clickInBasketClose}>
                        <img className="star__initial--close star__initial--pos-right" src={closeBasket} alt="close"/>
                    </button>
                    <button className="star__btn--initial star__btn--initial-pos--left" onClick={handleFavoriteAdd}>
                        <img className="star__initial star__initial--pos-left"  src={isFavorite ? heartPink : heartBlack} />
                    </button>
                </>
            } 
        </li>
    );
}   
    
    
}

export default Card;