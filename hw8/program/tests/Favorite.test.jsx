import favoriteReducer, { setFavoriteCard, deleteFavoriteCard } from '../src/redax/favoriteSlice';
import "@testing-library/jest-dom";

describe('favoriteSlice', () => {
  const initialState = {
    checkFavorite: [],
  };

  it('should handle initial state', () => {
    expect(favoriteReducer(undefined, { type: 'unknown' })).toEqual(initialState);
  });

  it('should handle setFavoriteCard', () => {
    const newState = favoriteReducer(initialState, setFavoriteCard({ id: 1, title: 'Test Product' }));
    expect(newState.checkFavorite).toEqual([{ id: 1, title: 'Test Product' }]);
  });

  it('should handle deleteFavoriteCard', () => {
    const stateWithItem = {
      checkFavorite: [{ id: 1, title: 'Test Product' }],
    };
    const newState = favoriteReducer(stateWithItem, deleteFavoriteCard(1));
    expect(newState.checkFavorite).toEqual([]);
  });

  it('should save to localStorage when setFavoriteCard is called', () => {
    jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
    favoriteReducer(initialState, setFavoriteCard({ id: 1, title: 'Test Product' }));
    expect(localStorage.setItem).toHaveBeenCalledWith("favoriteCard", JSON.stringify([{ id: 1, title: 'Test Product' }]));
  });

  it('should save to localStorage when deleteFavoriteCard is called', () => {
    const stateWithItem = {
      checkFavorite: [{ id: 1, title: 'Test Product' }],
    };
    jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
    favoriteReducer(stateWithItem, deleteFavoriteCard(1));
    expect(localStorage.setItem).toHaveBeenCalledWith("favoriteCard", JSON.stringify([]));
  });
});