import basketReducer, { setBasketCard, deleteBasketCard, clearBasket } from '../src/redax/basketSlice';
import "@testing-library/jest-dom";

describe('basketSlice', () => {
  const initialState = {
    checkBasket: [],
  };

  beforeEach(() => {
    localStorage.clear();
  });

  it('should handle initial state', () => {
    expect(basketReducer(undefined, { type: 'unknown' })).toEqual(initialState);
  });

  it('should handle setBasketCard', () => {
    const newState = basketReducer(initialState, setBasketCard({ id: 1, title: 'Test Product' }));
    expect(newState.checkBasket).toEqual([{ id: 1, title: 'Test Product' }]);
  });

  it('should handle deleteBasketCard', () => {
    const stateWithItem = {
      checkBasket: [{ id: 1, title: 'Test Product' }],
    };
    const newState = basketReducer(stateWithItem, deleteBasketCard(1));
    expect(newState.checkBasket).toEqual([]);
  });

  it('should handle clearBasket', () => {
    const stateWithItems = {
      checkBasket: [{ id: 1, title: 'Test Product' }, { id: 2, title: 'Another Product' }],
    };
    const newState = basketReducer(stateWithItems, clearBasket());
    expect(newState.checkBasket).toEqual([]);
  });

  it('should save to localStorage when setBasketCard is called', () => {
    jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
    basketReducer(initialState, setBasketCard({ id: 1, title: 'Test Product' }));
    expect(localStorage.setItem).toHaveBeenCalledWith("basketCard", JSON.stringify([{ id: 1, title: 'Test Product' }]));
  });

  it('should save to localStorage when deleteBasketCard is called', () => {
    const stateWithItem = {
      checkBasket: [{ id: 1, title: 'Test Product' }],
    };
    jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
    basketReducer(stateWithItem, deleteBasketCard(1));
    expect(localStorage.setItem).toHaveBeenCalledWith("basketCard", JSON.stringify([]));
  });

  it('should remove from localStorage when clearBasket is called', () => {
    const stateWithItems = {
      checkBasket: [{ id: 1, title: 'Test Product' }, { id: 2, title: 'Another Product' }],
    };
    jest.spyOn(Object.getPrototypeOf(window.localStorage), 'removeItem');
    basketReducer(stateWithItems, clearBasket());
    expect(localStorage.removeItem).toHaveBeenCalledWith("basketCard");
  });
});