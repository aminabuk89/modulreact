import { createBrowserRouter } from "react-router-dom";
import Layout from "./components/Layout";
import Home from "./pages/Home";
import Basket from "./pages/Basket";
import Favorite from "./pages/Favorite";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Layout/>,
        children: [
            {
                index: true,
                element: <Home/>
            },
            {
                path: "/basket",
                element : <Basket/>
            },
            {
                path: "/favorite",
                element : <Favorite/>
            }
        ]
    }
])
export default router;