import { configureStore } from "@reduxjs/toolkit";
import goodsReducer from "./goodsSlice";
import modalReducer from "./modalSlice";
import basketReducer from "./basketSlice";
import favoriteReducer from "./favoriteSlice";

const store = configureStore({
  reducer: {
    goods: goodsReducer,
    modal: modalReducer,
    basket: basketReducer,
    favorite: favoriteReducer,
  },
});

export default store;
