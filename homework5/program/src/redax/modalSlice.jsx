import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  modalOpen: false,
  selectedProductId: null,
  modalType: null,
};

const modalSlice = createSlice({
  name: 'modal',
  initialState,
  reducers: {
    openModal: (state, action) => {
      state.modalOpen = true;
      state.selectedProductId = action.payload.productId;
      state.modalType = action.payload.modalType;
    },
    closeModal: (state) => {
      state.modalOpen = false;
      state.selectedProductId = null;
      state.modalType = null;
    },
  },
});


export const getStateModal = (state) => state.modal;
export const { openModal, closeModal } = modalSlice.actions;
export default modalSlice.reducer;