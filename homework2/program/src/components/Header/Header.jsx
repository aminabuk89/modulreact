import PropTypes from 'prop-types';

const Header = ({basketCounter, favoriteCounter}) => {

    return (
        <div className="page__header">
            <h1 className="page__title">Асортимент жіночого магазина</h1>
            <button className="page__btn--basket">
                <svg width="40" height="40" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M2.5 3.33325H3.00526C3.85578 3.33325 4.56986 3.97367 4.6621 4.81917L5.3379 11.014C5.43014 11.8595 6.14422 12.4999 6.99474 12.4999H14.205C14.9669 12.4999 15.6317 11.9833 15.82 11.2451L16.9699 6.73584C17.2387 5.68204 16.4425 4.65733 15.355 4.65733H5.5M5.52063 15.5207H6.14563M5.52063 16.1457H6.14563M14.6873 15.5207H15.3123M14.6873 16.1457H15.3123M6.66667 15.8333C6.66667 16.2935 6.29357 16.6666 5.83333 16.6666C5.3731 16.6666 5 16.2935 5 15.8333C5 15.373 5.3731 14.9999 5.83333 14.9999C6.29357 14.9999 6.66667 15.373 6.66667 15.8333ZM15.8333 15.8333C15.8333 16.2935 15.4602 16.6666 15 16.6666C14.5398 16.6666 14.1667 16.2935 14.1667 15.8333C14.1667 15.373 14.5398 14.9999 15 14.9999C15.4602 14.9999 15.8333 15.373 15.8333 15.8333Z" stroke="#3C4242" strokeWidth="1.5" strokeLinecap="round"/>
                </svg>
                <p className="page__btn--counter">{ basketCounter.length}</p>
            </button>
            <button className='page__btn--favorite'>
                <svg className="star__favorite" width="45" height="45" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12.4362 5.90689L10.7175 2.91506C10.3983 2.35955 9.59787 2.35689 9.27507 2.91027L7.46145 6.01933C7.22123 6.43113 6.81616 6.72035 6.34868 6.81384L3.87058 7.30946C3.22876 7.43783 2.97867 8.22206 3.42768 8.69829L5.22004 10.5993C5.60331 11.0058 5.75741 11.5773 5.63046 12.1213L4.81918 15.5983C4.65241 16.313 5.42416 16.8787 6.05555 16.5045L9.15073 14.6703C9.67464 14.3599 10.3262 14.3599 10.8501 14.6703L13.9453 16.5045C14.5766 16.8787 15.3484 16.313 15.1816 15.5983L14.3632 12.0909C14.2402 11.5637 14.3809 11.0095 14.7406 10.6049L16.5352 8.58603C16.9628 8.105 16.7069 7.34146 16.0758 7.21524L13.5545 6.71098C13.0832 6.61673 12.6756 6.32362 12.4362 5.90689Z" stroke="#EDD146" strokeWidth="1.5" strokeMiterlimit="10"/>
                </svg>
                <p className='page__favorite--counter'>{favoriteCounter.length}</p>
            </button>
        </div>
    )
}

Header.propTypes = {
    basketCounter: PropTypes.array,
    favoriteCounter: PropTypes.array
}

export default Header;