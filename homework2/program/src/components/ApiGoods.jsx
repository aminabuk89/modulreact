import Header from "./Header/Header";
import ProductList from "./ProductList/ProductList";
import { useState, useEffect } from "react";

const ApiGoods = () => {

    const basketCard = localStorage.getItem("basketCard") ? JSON.parse(localStorage.getItem("basketCard")) : [];
    const favoriteCard = localStorage.getItem("favoriteCard") ? JSON.parse(localStorage.getItem("favoriteCard")) : [];
    
    const [basketCounter, setBasketCounter] = useState(basketCard);
    const [favoriteCounter, setFavoriteCounter] = useState(favoriteCard);

    const checkLocalBasket = () => {
        localStorage.setItem("basketCard", JSON.stringify(basketCounter))
    }
    useEffect(checkLocalBasket, [basketCounter]);

    const checkLocalFavorite = () => {
        localStorage.setItem("favoriteCard", JSON.stringify(favoriteCounter))
    }
    useEffect(checkLocalFavorite, [favoriteCounter]);


    const [showGoods, setShowGoods] = useState([]);
    const fetchGoods = () => {
        const url = "../goods.json";
        fetch(url)
            .then((response)=>response.json())
            .then((data) => setShowGoods(data))

    }
    useEffect(fetchGoods, []);
    // const heardCard = localStorage.getItem("heartFav") ? JSON.parse(localStorage.getItem("heartFav")) : [];
    // const [heart, setHeart] = useState(heardCard)

    // useEffect(() => {
    //     localStorage.setItem("heartFav", JSON.stringify(heart));
    // }, [heart]);


    return (
        <section className="section__main">
            <Header basketCounter={basketCounter} favoriteCounter={ favoriteCounter} />
            <ProductList 
            showGoods={showGoods}
            basketCard={ basketCard}
            setBasketCounter={setBasketCounter}
                setFavoriteCounter={setFavoriteCounter}
                basketCounter={basketCounter}
                favoriteCounter={favoriteCounter}
                // setHeart={setHeart}
                // heart={heart}
            />
        </section>
    )
}
export default ApiGoods;


