import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import heartBlack from "../../assets/icon-black.png";
import heartPink from "../../assets/icon-pink.png"

const Card = ({title, img, color, price, article, id, openModal, favorite, basketCounter, favoriteCounter, addToCardFavorite}) => {

    const [isInBasket, setIsInBasket] = useState(false);
    const [isInFavorite, setIsInFavorite] = useState(false)

     useEffect(() => {
            setIsInBasket(basketCounter.some(item => item.id === id));
            setIsInFavorite(favoriteCounter.some(item => item.id === id));
     }, [basketCounter, favoriteCounter, id]);


    const clickInBasket = () => {
        if (!isInBasket && !isInFavorite) {
            openModal(id);
        }
    }

    const clickInFavorite = () => {
        if (!isInBasket) {
            addToCardFavorite();
        }
    }

    return (
        <li className="card__item" >
            <h2 className="card__title">{title}</h2>
            <img className="card__photo" src={img} alt="photo goods" />
           <div className="card__text--container">
                <p className="card__text--color text">Колір: {color}</p>
                <span className="card__text--price text">Ціна: {price}</span>
                <p className="card__text--number text">Артикул: {article}</p>
            </div>
            <button className={isInBasket ? "card__button card__button--active" : "card__button card__button--posiv"} onClick={clickInBasket} disabled={isInBasket}>
                {isInBasket ? "Товар в кошику" : "Додати до кошика"}
            </button>
            <button className="star__btn--initial" onClick={clickInFavorite} disabled={isInFavorite}>
                <img className="star__initial" width="33" height="33" src={favorite ? heartPink : heartBlack} />
            </button>
        </li>
    )
}

Card.propTypes = {
    openModal: PropTypes.func,
    favorite: PropTypes.bool,
  
    title: PropTypes.string,
    img: PropTypes.string,
    color: PropTypes.string,
    price: PropTypes.string,
    article: PropTypes.number,
};

export default Card;
