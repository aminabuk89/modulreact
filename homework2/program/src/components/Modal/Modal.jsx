import PropTypes from 'prop-types';
import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModalFooter from "./ModalFooter";
import ModalBody from "./ModalBody";
import ModalOut from "./ModalOut";
import "./Modal.scss"

const Modal = ({ clickCloseModal, data, addToCardBasket}) => {

    return (
         <ModalOut clickCloseModal={clickCloseModal}>
            <ModalWrapper title={data.title}>
                <ModalHeader img={data.img} description={data.description}/>
                <ModalBody>
                            <ModalFooter
                                firstText="Додати товар до кошика"
                                addToCardBasket={addToCardBasket}
                                className="btn__card btn__card--first"
                            />
                            <ModalFooter
                                secondaryText="Не додавати"
                                clickCloseModal={clickCloseModal}
                                className="btn__card btn__card--second"
                            />
                </ModalBody>
            </ModalWrapper>
        </ModalOut>
    );
}

export default Modal;

Modal.propTypes = {
    clickCloseModal: PropTypes.func,
    data: PropTypes.object,
    addToCardBasket: PropTypes.func,
    addToCardFavorite: PropTypes.func,
    typeModal: PropTypes.string
}
