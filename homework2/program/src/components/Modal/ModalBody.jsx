import PropTypes from 'prop-types';
const ModalBody = ({ children }) => {
    
    return (
        <div className="card__body">{children} </div>
        )
}

export default ModalBody;

ModalBody.propTypes = {
    children: PropTypes.node
}