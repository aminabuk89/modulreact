import PropTypes from 'prop-types';

const ModalOut = ({ children, clickCloseModal }) => {
    
    const handleClickOutside = (e) => {
    if (clickCloseModal && e.target.classList.contains('card__modal')) {
      clickCloseModal();
    }
  };

    return (
        <div className="card__modal" onClick={handleClickOutside}>{ children}</div>
    )
}
export default ModalOut;

ModalOut.propTypes = {
  children: PropTypes.node,
  clickCloseModal: PropTypes.func
}