import PropTypes from 'prop-types';

const ModalWrapper = ({children, title}) => {
    return (
        <div className="modal__window">
            <h2 className="card__title">{title}</h2>
            {children}
        </div>
    )
}
export default ModalWrapper;


ModalWrapper.propTypes = {
    children: PropTypes.node,
    title: PropTypes.string
}




