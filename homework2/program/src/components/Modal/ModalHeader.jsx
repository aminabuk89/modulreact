import PropTypes from 'prop-types';

const ModalHeader = ({  description, img }) => {
    return (
        <div className="card__container">
            <img className="card__photo card__photo--modal" src={img} alt="photo goods" />
            <p className="card__description">
                <span className="card__description card__description--title">Детальніше про товар:  </span>
                {description}
            </p>
        </div>
        )
}

export default ModalHeader;

ModalHeader.propTypes = {
    description: PropTypes.string,
    img: PropTypes.string
}