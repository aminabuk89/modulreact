import PropTypes from 'prop-types';

const ModalFooter = ({ firstText,  clickCloseModal, className, secondaryText, addToCardBasket, text }) => {
  
  return (

    <div className="modal__footer">

      {firstText &&  (
        <button onClick={addToCardBasket} className={className}>{firstText}</button>
      )}

      {secondaryText && (
        <button onClick={clickCloseModal} className={className}>{secondaryText}</button>
      )}
    </div>
  );
};

export default ModalFooter;

ModalFooter.propTypes = {
  firstText: PropTypes.string,
  clickCloseModal: PropTypes.func,
  className: PropTypes.string,
  secondaryText: PropTypes.string,
  addToCardBasket: PropTypes.func,
  addToCardFavorite: PropTypes.func,
  text: PropTypes.string
}