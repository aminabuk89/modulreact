import { useState, useEffect } from "react";
import Card from "../MainPage/Card";
import Modal from "../Modal/Modal";
import PropTypes from 'prop-types';

const ProductList = ({ showGoods,  setBasketCounter, setFavoriteCounter, basketCounter, favoriteCounter}) => {

    const [showModal, setShowModal] = useState(false);
    const [selectedId, setSelectedId] = useState(null);

    const addToCardBasket = () => {
        const data  = showGoods.find(item => item.id === selectedId)
                if (!data) {
            console.error('Item not found');
            return;
        }
            setBasketCounter(prev => {
                const checkId = prev.some(item => item.id === data.id);
                if (!checkId) {
                    const newBasketCard = [...prev, data];
                    return newBasketCard;
                }
                 return prev; 
            });
            clickCloseModal();      
    };

    const [heart, setHeart] = useState([])

    const addToCardFavorite = () => {
     
        const data = showGoods.find(item => item.id === selectedId);
            if (!data) {
            console.error('Item not found');
            return;
        }
        setFavoriteCounter(prev => {
            const checkId = prev.some(item => item.id === data.id);
            if (!checkId) {
                const newFavoriteCard = [...prev, data];
                setHeart([...heart, data.id]);
                return newFavoriteCard;
            } else {
                const updatedFavorites = prev.filter(item => item.id !== data.id);
                setHeart(heart.filter(id => id !== data.id));
                return updatedFavorites;
            }
        });
        clickCloseModal();    
    };


    // const [typeModal, setTypeModal] = useState(null);

    const clickCloseModal = () => {
        setShowModal(false);
    };

    const openModal = (id) => {
        setSelectedId(id);
        setShowModal(true);
        // setTypeModal(type)
    };

    return (
        <>
            <ul className="card__list">
                {showGoods.map((data, index) => {
                    return (
                        <Card
                            key={index}
                            id={data.id}
                            data={data}
                            title={data.title}
                            img={data.img}
                            color={data.color}
                            price={data.price}
                            article={data.article}
                            setShowModal={setShowModal}
                            openModal={() => openModal(data.id)}
                            addToCardFavorite={addToCardFavorite}
                            heart={heart}
                            favorite={heart.includes(data.id)}
                            basketCounter={basketCounter}
                            favoriteCounter={favoriteCounter}
                        />
                    )
                })
                }
            </ul>
            {showModal && selectedId  && (
                <Modal
                    clickCloseModal={clickCloseModal}
                    data={showGoods.find((item) => item.id === selectedId)}
                    addToCardBasket={addToCardBasket}
                    addToCardFavorite={addToCardFavorite}
                   
                 
                />
            )}
        </>
    );
}
export default ProductList;

ProductList.propTypes = {
    showGoods: PropTypes.array,
    setBasketCounter: PropTypes.func,
    setFavoriteCounter:  PropTypes.func
}