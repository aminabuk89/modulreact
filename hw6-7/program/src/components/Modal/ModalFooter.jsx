import { setFavoriteCard, deleteFavoriteCard } from "../../redax/favoriteSlice";
import { useDispatch, useSelector } from 'react-redux';
import { getStateModal } from "../../redax/modalSlice";
import { setBasketCard, deleteBasketCard } from "../../redax/basketSlice";


const ModalFooter = ({ firstText, className, secondaryText,  text, textBasketClose, textFavoriteClose, saveGoods, handleCloseModal }) => {

  const { selectedProductId } = useSelector(getStateModal);

    const dispatch = useDispatch();
    const goods = useSelector((state) => state.goods.goods);

      const addToCardBasket = () => {
        const data = goods.find(item => item.id === selectedProductId)
        if (data) {
            dispatch(setBasketCard(data))
        }

        handleCloseModal();      
      };

      const addToCardFavorite = () => {
        const data = goods.find(item => item.id === selectedProductId);

        if (data) {
            dispatch(setFavoriteCard(data));
        };

        handleCloseModal();      
      };

      const deleteCardBasket = () => {
        dispatch(deleteBasketCard(selectedProductId))
        handleCloseModal();
      }

      const deleteCardFavorite = () => {
        dispatch(deleteFavoriteCard(selectedProductId))
        handleCloseModal();
      }
  
      
  return (

    <div className="modal__footer">

      {firstText &&  (
        <button onClick={addToCardBasket} className={className}>{firstText}</button>
      )}

      {text && (
        <button onClick={addToCardFavorite} className={className}>{text}</button>
      )}

      {secondaryText &&  (
        <button onClick={handleCloseModal} className={className}>{secondaryText}</button>
      )}
      {textBasketClose && (
        <button onClick={deleteCardBasket} className={className}>{textBasketClose}</button>
      )}
      {textFavoriteClose && (
        <button onClick={deleteCardFavorite} className={className}>{textFavoriteClose}</button>
      )}
      {saveGoods &&  (
        <button onClick={handleCloseModal} className={className}>{saveGoods}</button>
      )}

   </div>
  );
};

export default ModalFooter;

