import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { clearBasket } from '../../redax/basketSlice';
import { useDispatch } from 'react-redux';


const validationSchema = Yup.object({
    
    firstName: Yup.string()
        .required("Ім'я є обов'язковим")
        .min(3, "Ім'я повинно мати мінімум 3 літери"),
    lastName: Yup.string()
        .required("Прізвище є обов'язковим")
        .min(4, "Прізвище повинно мати мінімум 3 літери"),
    age: Yup.number()
        .required("Вік є обов'язковим")
        .positive("Вік повинен бути позитивним числом")
        .integer("Вік повинен бути цілим числом")
        .min(16, "Вік повинен бути не менше 16 років")
        .max(75, "Вік повинен бути не більше 75 років"),
    phone: Yup.string()
        .matches(/^\+380\d{9}$/, "Номер телефону повинен починатися з +380 і містити 9 цифр після коду країни")
        .required("Номер телефону є обов'язковим"),
    city: Yup.string()
        .required("Місто є обов'язковим")
        .min(4, "Місто повинно мати мінімум 4 літери"),
    street: Yup.string()
        .required("Вулиця є обов'язковою"),
    home: Yup.number()
        .required("Номер будинку є обов'язковим")
        .positive("Номер будинку повинен бути позитивним числом")
        .integer("Номер будинку повинен бути цілим числом")
});

const SignupForm = () => {
    const dispatch = useDispatch();
    return (
        
        <Formik
            initialValues={{
                firstName: '',
                lastName: '',
                age: "",
                phone: "",
                city: "",
                street: "",
                home: ""
            }}
            validationSchema={validationSchema }
            validateOnBlur={true}
            validateOnChange={true}
            onSubmit={(values, { resetForm }) => {
                
                console.log(`Дані для отримання товарів:`, values);

                const isArrayBasketCard = localStorage.getItem("basketCard");
                if (isArrayBasketCard) {
                    const cartItems = JSON.parse(isArrayBasketCard);
                    console.log('Куплені товари:', cartItems);
                    dispatch(clearBasket()); 
                }
                resetForm();
            }}
        >
        
            <Form className='form'>
                <h2 className="subtitle__page subtitle__page--center">Заповніть інформацію для доставки товарів</h2>

                <div className='form__wrapper'>
                    <div className='form__container'>
                        <label htmlFor="firstName" className='form__label'>Ваше ім'я</label>
                        <Field className="form__field" type="text" name="firstName" placeholder="Введіть, будь ласка, ім'я"/>
                        <ErrorMessage name="firstName" component="div" className="error"/>
                    </div>

                    <div className='form__container'>
                        <label htmlFor="lastName" className='form__label'>Ваше прізвище</label>
                        <Field className="form__field" type="text" name="lastName" placeholder="Введіть, будь ласка, прізвище"/>
                        <ErrorMessage name="lastName" component="div" className="error"/>
                    </div>

                    <div className='form__container'>
                        <label htmlFor="age" className='form__label'>Ваш вік</label>
                        <Field className="form__field" type="number" name="age" placeholder="Введіть, будь ласка, повний вік"/>
                        <ErrorMessage name="age" component="div" className="error"/>
                    </div>

                    <div className='form__container'>
                        <label htmlFor="phone" className='form__label'>Ваш номер телефону</label>
                        <Field className="form__field" type="tel" name="phone" placeholder="Введіть, будь ласка, номер телефону"/>
                        <ErrorMessage name="phone" component="div" className="error" />
                    </div>

                    <p className='form__label'>Інформація про доставку</p>
            
                    <Field className="form__field" type="text" name="city" placeholder="Введіть, будь ласка, місто/село"/>
                    <ErrorMessage name="city" component="div" className="error"/>

                    <Field className="form__field" type="text" name="street" placeholder="Введіть, будь ласка, вулицю"/>
                    <ErrorMessage name="street" component="div" className="error"/>

                    <Field className="form__field" type="number" name="home" placeholder="Введіть, будь ласка, номер будинку"/>
                    <ErrorMessage name="home" component="div" className="error"/>

                    <button type='submit' className='form__btn'>Відправити</button>
                </div>
            </Form>
        </Formik>
    )
}

export default SignupForm;