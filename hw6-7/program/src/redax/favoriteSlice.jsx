import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    checkFavorite: localStorage.getItem("favoriteCard") ? JSON.parse(localStorage.getItem("favoriteCard")) : []
}

const favoriteSlice = createSlice({
    name: "favorite",
    initialState,
    reducers: {
        setFavoriteCard: (state, action) => {
            state.checkFavorite.push(action.payload);
            localStorage.setItem("favoriteCard", JSON.stringify(state.checkFavorite))
        },
        deleteFavoriteCard: (state, action) => {
            state.checkFavorite = state.checkFavorite.filter((item)=> item.id !== action.payload)
            localStorage.setItem("favoriteCard", JSON.stringify(state.checkFavorite));  
        }
    }
})

export default favoriteSlice.reducer;
export const getFavoriteCard = (state) => state.favorite.checkFavorite;
export const { setFavoriteCard, deleteFavoriteCard } = favoriteSlice.actions;