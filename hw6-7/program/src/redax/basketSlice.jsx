import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    checkBasket: localStorage.getItem("basketCard") ? JSON.parse(localStorage.getItem("basketCard")) : []
}

const basketSlice = createSlice({
    name: "basket",
    initialState,
    reducers: {
        setBasketCard: (state, action) => {
            state.checkBasket.push(action.payload);
            localStorage.setItem("basketCard", JSON.stringify(state.checkBasket))
        },
        deleteBasketCard: (state, action) => {
            state.checkBasket = state.checkBasket.filter((item)=> item.id !== action.payload)
            localStorage.setItem("basketCard", JSON.stringify(state.checkBasket));
            
        },
        clearBasket: (state) => {
            state.checkBasket = [];
            localStorage.removeItem("basketCard");
        }
    }
})

export default basketSlice.reducer;
export const getBasketCard = (state) => state.basket.checkBasket;
export const { setBasketCard, deleteBasketCard, clearBasket } = basketSlice.actions;