import ProductList from "../components/ProductList/ProductList";
import Modal from "../components/Modal/Modal";
import { useSelector } from "react-redux";
import { getStateModal } from "../redax/modalSlice";
import {getFavoriteCard} from "../redax/favoriteSlice";

const Favorite = () => {

    const { modalOpen, selectedProductId  } = useSelector(getStateModal);
    const arrayFavorite = useSelector(getFavoriteCard);
    
    return (
    <>
        <h2 className="subtitle__page subtitle__page--center">Улюблені товари</h2>

        <ProductList data={arrayFavorite} />
      
        {modalOpen && (
            <Modal data={arrayFavorite.find((item) => item.id === selectedProductId)}/>
        )}
    </>
    )
}

export default Favorite;