import ProductList from "../components/ProductList/ProductList";
import Modal from "../components/Modal/Modal";
import { useSelector } from "react-redux";
import { getStateModal } from "../redax/modalSlice";
import { getBasketCard } from "../redax/basketSlice";
import SignupForm from "../components/Form/Form";
import { useEffect, useState } from "react";

const Basket = () => {

    const { modalOpen, selectedProductId  } = useSelector(getStateModal);
    const arrayBasket = useSelector(getBasketCard);

    const [showDelivery, setShowDelivery] = useState(false);

    useEffect(() => {
        setShowDelivery(arrayBasket.length > 0);
    }, [arrayBasket]);

    return (
        <>
        <div className={showDelivery ? "main__wrapper" : "main__container"}>
            <div className="flex__element">
                 <h2 className={showDelivery ? "subtitle__page" : "subtitle__page subtitle__page--center"}>Товари з корзини</h2>
                <ProductList data={arrayBasket}/>
            </div>
            {showDelivery ?<SignupForm /> : null }
            
        </div>
            
        {modalOpen &&  (
            <Modal data={arrayBasket.find((item) => item.id === selectedProductId)}/>
        )}
    </>
    )
}
export default Basket;