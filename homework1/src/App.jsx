`use strict`;

import { useState } from "react";
import Button from "./components/Button/Button";
import ModalImage from "./components/ModalImage/ModalImage";
import photoCard from "./assets/cot.avif";
import ModalText from "./components/ModalText/ModalText";

const App = () => {
    const titleFirstBtn = `Open first modal`;
    const titleSecondBtn = `Open second modal`;

    const [modalType, setModalType] = useState(null);
    const [modalShow, setModalShow] = useState(false);

    const openModal = (type) => {
        setModalType(type);
        setModalShow(true);
    };

    const closeModal = () => {
        setModalShow(false);
    };


    const [showCard, setShowCard] = useState(true)

    const clickStateModal = () => {
        setShowCard(showCard);
    }

    return (
        <section className="main__section">
            <div className="section__container--btn">
                <Button className="btn section__btn--first" type="button" onClick={() => openModal('image')}>{titleFirstBtn}</Button>
                <Button className="btn section__btn--second" type="button" onClick={() => openModal('text')}>{titleSecondBtn}</Button>
            </div>
            {modalShow && modalType === 'image' && <ModalImage image={photoCard} closeModal={closeModal} showCard={showCard} clickStateModal={ clickStateModal} />}
            {modalShow && modalType === 'text' && <ModalText text="Add Product “NAME”" closeModal={ closeModal} showCard={showCard} clickStateModal={ clickStateModal}/>}
        </section>
    )
}
export default App;
