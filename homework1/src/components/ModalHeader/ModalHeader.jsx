`use strict`;
import "./ModalHeader.scss"

const ModalHeader = ({ children }) => {
    return (
        <div className="card__title">{children}</div>
        )
}

export default ModalHeader;