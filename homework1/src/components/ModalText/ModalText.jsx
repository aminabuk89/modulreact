import ModalWrapper from "../ModalWrapper/ModalWrapper";
import ModalHeader from "../ModalHeader/ModalHeader";
import ModalFooter from "../ModalFooter/ModalFooter";
import ModalClose from "../ModalClose/ModalClose";
import ModalBody from "../ModalBody/ModalBody";
import Modal from "../Modal/Modal";
import "./ModalText.scss"

const ModalText = ({ text, closeModal, showCard }) => {

    return (
        <>
            {showCard && (
                <Modal closeModal={closeModal}>
                    <ModalWrapper>
                        <p className="card__text--title">{ text}</p>
                        <ModalHeader>
                            <ModalClose closeModal={closeModal} />
                            <p className="card__text">Description for you product.</p>
                        </ModalHeader>
                        <ModalBody >
                            <ModalFooter firstText="ADD TO FAVORITE" firstClick={closeModal}  className="btn__card btn__card--first"/>
                        </ModalBody>
                    </ModalWrapper>
                </Modal>
            )}
        </>
    )
}

export default ModalText;