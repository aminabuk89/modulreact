`use strict`;
import "./ModalBody.scss"
const ModalBody = ({ children }) => {
    
    return (
        <div className="card__body">{children} </div>
        )
}

export default ModalBody;