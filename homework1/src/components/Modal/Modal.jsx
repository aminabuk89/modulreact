`use strict`;

import "./Modal.scss";

const Modal = ({ children, closeModal }) => {
    
    const handleClickOutside = (e) => {
    if (closeModal && e.target.classList.contains('card__modal')) {
      closeModal();
    }
  };

    return (
        <div className="card__modal" onClick={handleClickOutside}>{ children}</div>
    )
}
export default Modal;