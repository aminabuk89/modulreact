`use strict`;

import ModalWrapper from "../ModalWrapper/ModalWrapper";
import ModalHeader from "../ModalHeader/ModalHeader";
import ModalFooter from "../ModalFooter/ModalFooter";
import ModalClose from "../ModalClose/ModalClose";
import ModalBody from "../ModalBody/ModalBody";
import Modal from "../Modal/Modal";
import "./ModalImage.scss"

const ModalImage = ({ image, closeModal, clickStateModal, showCard}) => {

    return (
        <>  
            {showCard && (
                <Modal closeModal={closeModal}>
                    <ModalWrapper>
                        <img className="card__photo" src={image} alt="photo"/>
                        <ModalHeader>
                            <ModalClose closeModal={closeModal} />
                            <h2 className="card__title">Product Delete!</h2>
                            <p className="card__text">By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</p>
                        </ModalHeader>
                        <ModalBody >
                            <ModalFooter firstText="NO, CANSEL"  firstClick={clickStateModal} className="btn__card btn__card--first" />
                            <ModalFooter secondaryText="YES, DELETE" secondaryClick={closeModal}  className="btn__card btn__card--second"/>
                        </ModalBody>
                    </ModalWrapper>
                </Modal>
            )}
        </>
    )
}


export default ModalImage;