import "./ModalFooter.scss"

const ModalFooter = ({ firstText, secondaryText, firstClick, secondaryClick, className }) => {
  
  return (
    <div className="modal__footer">
      {firstText && firstClick && (
        <button onClick={firstClick} className={className}>{firstText}</button>
      )}
      {secondaryText && secondaryClick && (
        <button onClick={secondaryClick} className={className}>{secondaryText}</button>
      )}
    </div>
  );
};

export default ModalFooter;