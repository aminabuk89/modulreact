`use strict`;

import "./ModalWrapper.scss"

const ModalWrapper = ({children}) => {
    return (
        <div className="modal__window">{ children}</div>
    )
}
export default ModalWrapper;







