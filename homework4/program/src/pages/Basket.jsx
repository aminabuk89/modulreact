import ProductList from "../components/ProductList/ProductList";
import Modal from "../components/Modal/Modal";
import { useSelector } from "react-redux";
import { getStateModal } from "../redax/modalSlice";
import { getBasketCard } from "../redax/basketSlice";

const Basket = () => {

const { modalOpen, selectedProductId  } = useSelector(getStateModal);
   const  arrayBasket  = useSelector(getBasketCard);
    return (
    <>
        <h2 className="subtitle__page">Товари з корзини</h2>

        <ProductList data={arrayBasket}/>
       
        {modalOpen &&  (
            <Modal data={arrayBasket.find((item) => item.id === selectedProductId)}/>
        )}
    </>
    )
}
export default Basket;