import React from 'react';
import ReactDOM from 'react-dom/client';
import "../src/index.scss";
import { RouterProvider } from 'react-router-dom';
import router from './router';
import store from './redax/store';
import { Provider } from 'react-redux';

ReactDOM.createRoot(document.getElementById('root')).render(
        <Provider store={store}>
                <RouterProvider router={router}/>
        </Provider>
)

