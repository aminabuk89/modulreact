

const ModalWrapper = ({ children, title }) => {
    
    return (
        <div className="modal__window">
            <h2 className="card__title">{title}</h2>
            {children}
        </div>
    )
}
export default ModalWrapper;







