
const ModalOut = ({ children, handleCloseModal}) => {

    
    const handleClickOutside = (e) => {
    if (handleCloseModal && e.target.classList.contains('card__modal')) {
      handleCloseModal();
    }
  };

    return (
        <div className="card__modal" onClick={handleClickOutside}>{ children}</div>
    )
}
export default ModalOut;

