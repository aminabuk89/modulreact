import Header from "./Header/Header";
import { Outlet } from "react-router-dom";
import Footer from "./Footer/Footer";

const Layout = () => {

    return (

        <section className="section__main">
            <Header   />
                <main>
                    <Outlet /> 
                </main>
            <Footer/>
        </section>

    )
}
export default Layout;


