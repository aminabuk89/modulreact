import goodsReducer, { fetchGoods } from '../src/redax/goodsSlice';
import "@testing-library/jest-dom";


describe('goodsSlice reducer', () => {
  test('should return the initial state', () => {
    const initialState = {
      goods: [],
      status: 'idle',
      error: null,
    };

    expect(goodsReducer(undefined, {})).toEqual(initialState);
  });

  test('should handle fetchGoods.pending', () => {
    const state = {
      goods: [],
      status: 'idle',
      error: null,
    };

    const nextState = goodsReducer(state, fetchGoods.pending());
    expect(nextState.status).toEqual('loading');
  });

  test('should handle fetchGoods.fulfilled', () => {
    const state = {
      goods: [],
      status: 'loading',
      error: null,
    };

    const payload = [{
    "id": 1,
    "title": "Product 1",
    "price": "119 UAH",
    "img": "./photo.avif",
    "article": 456789,
    "color": "чорний",
    }];
      
    const nextState = goodsReducer(state, fetchGoods.fulfilled(payload));
    
    expect(nextState.status).toEqual('succeeded');
    expect(nextState.goods).toEqual(payload);
  });

  test('should handle fetchGoods.rejected', () => {
    const state = {
      goods: [],
      status: 'loading',
      error: null,
    };

    const errorMessage = 'Failed to fetch goods.';
    const nextState = goodsReducer(state, fetchGoods.rejected(new Error(errorMessage)));

    expect(nextState.status).toEqual('failed');
    expect(nextState.error).toEqual(errorMessage);
  });
});