import modalReducer, { openModal, closeModal } from '../src/redax/modalSlice';
import "@testing-library/jest-dom";

describe('modalSlice', () => {
  const initialState = {
    modalOpen: false,
    selectedProductId: null,
    modalType: null,
  };

  it('should handle initial state', () => {
    expect(modalReducer(undefined, { type: 'unknown' })).toEqual(initialState);
  });

  it('should handle openModal', () => {
    const action = { payload: { productId: 1, modalType: 'basket' } };
    const newState = modalReducer(initialState, openModal(action));
    expect(newState).toEqual({
      modalOpen: true,
      selectedProductId: 1,
      modalType: 'basket',
    });
  });

  it('should handle closeModal', () => {
    const stateWithModalOpen = {
      modalOpen: true,
      selectedProductId: 1,
      modalType: 'basket',
    };
    const newState = modalReducer(stateWithModalOpen, closeModal());
    expect(newState).toEqual(initialState);
  });
});