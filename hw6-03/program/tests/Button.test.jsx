import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import configureStore from 'redux-mock-store';
import Card from '../src/components/Card/Card';
import { openModal } from '../src/redax/modalSlice';
import "@testing-library/jest-dom";


const mockStore = configureStore([]);

describe('Card component', () => {
  let store;
  let initialState;

  beforeEach(() => {
    initialState = {
      basket: { items: [] },
      favorite: { items: [] },
    };
    store = mockStore(initialState);
    store.dispatch = jest.fn();
  });

  const renderCard = (props, route = '/') => {
    window.history.pushState({}, 'Test page', route);
    return render(
      <Provider store={store}>
        <Router>
          <Card {...props} />
        </Router>
      </Provider>
    );
  };

  it('renders Card component with correct data', () => {
    renderCard({ title: 'Test Product', img: 'test.jpg', color: 'Red', price: 100, article: '12345', id: 1 });
    expect(screen.getByText('Test Product')).toBeInTheDocument();
    expect(screen.getByText(/Ціна: 100/)).toBeInTheDocument();
  });

  it('renders "Додати до кошика" button on the home page and dispatches action when clicked', () => {
    renderCard({ title: 'Test Product', img: 'test.jpg', color: 'Red', price: 100, article: '12345', id: 1 });
    const addToBasketButton = screen.getByText('Додати до кошика');
    expect(addToBasketButton).toBeInTheDocument();
    fireEvent.click(addToBasketButton);
    expect(store.dispatch).toHaveBeenCalledWith(openModal({ productId: 1, modalType: 'basket' }));
  });

  it('renders "Товар в кошику" button on the home page if item is in the basket', () => {
    initialState.basket.items = [{ id: 1 }];
    store = mockStore(initialState);
    renderCard({ title: 'Test Product', img: 'test.jpg', color: 'Red', price: 100, article: '12345', id: 1 });
    expect(screen.getByText('Товар в кошику')).toBeInTheDocument();
  });

  it('renders "close" button on the favorite page and dispatches action when clicked', () => {
    renderCard({ title: 'Test Product', img: 'test.jpg', color: 'Red', price: 100, article: '12345', id: 1 }, '/favorite');
    const closeFavoriteButton = screen.getByAltText('close');
    expect(closeFavoriteButton).toBeInTheDocument();
    fireEvent.click(closeFavoriteButton);
    expect(store.dispatch).toHaveBeenCalledWith(openModal({ productId: 1, modalType: 'favoriteClose' }));
  });

  it('renders buttons correctly on the basket page and dispatches action when clicked', () => {
    renderCard({ title: 'Test Product', img: 'test.jpg', color: 'Red', price: 100, article: '12345', id: 1 }, '/basket');
    const closeBasketButton = screen.getByAltText('close');
    expect(closeBasketButton).toBeInTheDocument();
    fireEvent.click(closeBasketButton);
    expect(store.dispatch).toHaveBeenCalledWith(openModal({ productId: 1, modalType: 'basketClose' }));

    const favoriteButton = screen.getByAltText('heart');
    expect(favoriteButton).toBeInTheDocument();
    fireEvent.click(favoriteButton);
    expect(store.dispatch).toHaveBeenCalledWith(openModal({ productId: 1, modalType: 'favorite' }));
  });
});
