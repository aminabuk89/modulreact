import ProductList from "../components/ProductList/ProductList";
import Modal from "../components/Modal/Modal";
import { useSelector } from "react-redux";
import { getStateModal } from "../redax/modalSlice";
import {getFavoriteCard} from "../redax/favoriteSlice";
import DataContaxt from "../context/DataContaxt";
import { ViewProvider } from '../context/ViewProvider';
import ViewSwitcher from '../context/ViewSwitcher';

const Favorite = () => {

    const { modalOpen, selectedProductId  } = useSelector(getStateModal);
    const arrayFavorite = useSelector(getFavoriteCard);
    
    const data = arrayFavorite.find((item) => item.id === selectedProductId);

    return (
    <ViewProvider>
            <h2 className="subtitle__page subtitle__page--center">Улюблені товари</h2>
            <ViewSwitcher />
        <ProductList data={arrayFavorite} />
            
        <DataContaxt.Provider value={data}>
                {modalOpen && <Modal />}
        </DataContaxt.Provider>
    </ViewProvider>
    )
}

export default Favorite;