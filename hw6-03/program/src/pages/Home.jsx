import Modal from "../components/Modal/Modal";
import { useDispatch } from "react-redux";
import React, { useEffect } from 'react';
import ProductList from '../components/ProductList/ProductList';
import {useSelector } from "react-redux";
import { getStateModal } from "../redax/modalSlice";
import { fetchGoods } from "../redax/goodsSlice";
import DataContaxt from "../context/DataContaxt";
import { ViewProvider } from '../context/ViewProvider';
import ViewSwitcher from '../context/ViewSwitcher';

const Home = () => {

  const { modalOpen, selectedProductId } = useSelector(getStateModal);
  const dispatch = useDispatch();
  
  const goods = useSelector((state) => state.goods.goods);
  const status = useSelector((state) => state.goods.status);
  const error = useSelector((state) => state.goods.error);

  useEffect(() => {
    if (status === 'idle') {
        dispatch(fetchGoods());
    }
  }, [dispatch, status]);


  if (status === 'loading') {
    return <div className="text__desc--nothing">Завантаження...</div>;
  } else if (status === 'failed') {
    return <div>{error}</div>;
  }

  const data = goods.find((item) => item.id === selectedProductId)

  return (
    <ViewProvider>
      <h2 className="subtitle__page subtitle__page--center">Асортимент нашого магазину</h2>
      <ViewSwitcher />
      <ProductList data={goods} />

      <DataContaxt.Provider value={data}>
          {modalOpen && <Modal />}
      </DataContaxt.Provider>
      
    </ViewProvider>
  );
};

export default Home;