import Card from "../Card/Card";
import { ViewContext } from '../../context/ViewProvider';
import React, { useContext } from 'react';

const ProductList = ({ data }) => {
    
    const { view } = useContext(ViewContext);

    if ( data.length === 0) {
        return <p className="text__desc--nothing">Збережених товарів немає</p>; 
    }


    let productList;
    if (view === "grid") {
        productList = (
            <ul className='card__list'>
                {data.map((product, index) => (
                    <Card
                        key={index}
                        id={product.id}
                        title={product.title}
                        img={product.img}
                        color={product.color}
                        price={product.price}
                        article={product.article}
                    />
                ))}
            </ul>
        );
    } else {
        productList = (
            <table className="card__table">
              
                
                {data.map((product, index) => (
                    <Card
                        key={index}
                        id={product.id}
                        title={product.title}
                        img={product.img}
                        color={product.color}
                        price={product.price}
                        article={product.article}
                    />
                ))}
           
            </table>
        );
    }

    return productList;
}
   
export default ProductList;
