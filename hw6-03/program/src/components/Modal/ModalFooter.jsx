import { setFavoriteCard, deleteFavoriteCard } from "../../redax/favoriteSlice";
import { useDispatch, useSelector } from 'react-redux';
import { getStateModal } from "../../redax/modalSlice";
import { setBasketCard, deleteBasketCard } from "../../redax/basketSlice";
import { useContext } from "react";
import CloseModalContaxt from "../../context/CloseModalContaxt";


const ModalFooter = ({ addToBasket, notAdd,  addToFavorite, deleteGoodsBasket, deleteGoodsFavorite, saveGoods }) => {

  const { selectedProductId } = useSelector(getStateModal);

  const dispatch = useDispatch();
  const goods = useSelector((state) => state.goods.goods);
  
  const handleCloseModal = useContext(CloseModalContaxt)

      const addToCardBasket = () => {
        const data = goods.find(item => item.id === selectedProductId)
        if (data) {
            dispatch(setBasketCard(data))
        }

        handleCloseModal();      
      };

      const addToCardFavorite = () => {
        const data = goods.find(item => item.id === selectedProductId);

        if (data) {
            dispatch(setFavoriteCard(data));
        };

        handleCloseModal();      
      };

      const deleteCardBasket = () => {
        dispatch(deleteBasketCard(selectedProductId))
        handleCloseModal();
      }

      const deleteCardFavorite = () => {
        dispatch(deleteFavoriteCard(selectedProductId))
        handleCloseModal();
      }
  
  
  return (

    <div className="modal__footer">

      {addToBasket &&  (
        <button onClick={addToCardBasket} className="btn__card btn__card--first">{addToBasket}</button>
      )}

      {addToFavorite && (
        <button onClick={addToCardFavorite} className="btn__card btn__card--first">{addToFavorite}</button>
      )}

      {notAdd &&  (
        <button onClick={handleCloseModal} className="btn__card btn__card--second">{notAdd}</button>
      )}
      {deleteGoodsBasket && (
        <button onClick={deleteCardBasket} className="btn__card btn__card--first">{deleteGoodsBasket}</button>
      )}
      {deleteGoodsFavorite && (
        <button onClick={deleteCardFavorite} className="btn__card btn__card--first">{deleteGoodsFavorite}</button>
      )}
      {saveGoods &&  (
        <button onClick={handleCloseModal} className="btn__card btn__card--second">{saveGoods}</button>
      )}

   </div>
  );
};

export default ModalFooter;

