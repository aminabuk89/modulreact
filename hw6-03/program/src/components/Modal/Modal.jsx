import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModalFooter from "./ModalFooter";
import ModalBody from "./ModalBody";
import ModalOut from "../Modal/ModalOut"
import "./Modal.scss"
import {useSelector } from "react-redux";
import { getStateModal, closeModal } from "../../redax/modalSlice";
import { useDispatch } from "react-redux";
import CloseModalContaxt from "../../context/CloseModalContaxt";


const Modal = () => {

    const dispatch = useDispatch();
    const { modalType } = useSelector(getStateModal);
    
   
    const handleCloseModal = () => {
        dispatch(closeModal());
    };

    return (
        <CloseModalContaxt.Provider value={handleCloseModal}>
         <ModalOut>
            <ModalWrapper >
                <ModalHeader/>
                <ModalBody>
                        {modalType === "basket" &&(
                        <>
                            <ModalFooter
                                addToBasket="Додати товар до кошика"
                            />
                            <ModalFooter
                                notAdd="Не додавати"
                            />
                        </>
                          )} 
                        {modalType === "favorite" && (
                        <>
                            <ModalFooter
                                addToFavorite="Додати товар в улюблене"
                            />
                            <ModalFooter
                                notAdd="Не додавати"
                            />
                        </>
                        )}
                        {modalType === "basketClose" && (
                        <>
                            <ModalFooter
                                deleteGoodsBasket="Видалити товар з корзини"
                        
                            />
                            <ModalFooter
                                saveGoods="Залишити товар"
                           
                            />
                        </>
                        )}
                        {modalType === "favoriteClose" && (
                        <>
                            <ModalFooter
                                deleteGoodsFavorite="Видалити товар з улюбленого"
                            
                            />
                            <ModalFooter
                                saveGoods="Залишити товар"
                               
                            />
                        </>
                        )}
                    
                </ModalBody>
            </ModalWrapper>
            </ModalOut>
        </CloseModalContaxt.Provider>
    );
}

export default Modal;


