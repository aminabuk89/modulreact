import { useContext } from "react";
import DataContaxt from "../../context/DataContaxt";

const ModalWrapper = ({ children }) => {

    const data = useContext(DataContaxt)
    
    return (
        <div className="modal__window">
            <h2 className="card__title">{data.title}</h2>
            {children}
        </div>
    )
}
export default ModalWrapper;







