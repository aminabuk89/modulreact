import React from 'react';
import ReactDOM from 'react-dom/client';
import "../src/index.scss";
import { RouterProvider } from 'react-router-dom';
import router from './router';


import { GoodsProvider } from './context/GoodsContext';
import { BasketProvider } from './context/BasketContext';
import { FavoriteProvider } from './context/FavoriteContext';
import { ModalProvider } from './context/ModalContext';

ReactDOM.createRoot(document.getElementById('root')).render(
  <GoodsProvider>
    <BasketProvider>
      <FavoriteProvider>
        <ModalProvider>
          <RouterProvider router={router} />
        </ModalProvider>
      </FavoriteProvider>
    </BasketProvider>
  </GoodsProvider>
);


// import React from 'react';
// import ReactDOM from 'react-dom';
// import '../src/index.scss';
// import { BrowserRouter as Router } from 'react-router-dom'; // Імпортуємо BrowserRouter як Router
// import router from './router'; // Ваш конфігураційний об'єкт з маршрутами

// import { GoodsProvider } from './context/GoodsContext';
// import { BasketProvider } from './context/BasketContext';
// import { FavoriteProvider } from './context/FavoriteContext';
// import { ModalProvider } from './context/ModalContext';

// ReactDOM.render(
//   <React.StrictMode>
//     <GoodsProvider>
//       <BasketProvider>
//         <FavoriteProvider>
//           <ModalProvider>
//             <Router>
//               {router}
//             </Router>
//           </ModalProvider>
//         </FavoriteProvider>
//       </BasketProvider>
//     </GoodsProvider>
//   </React.StrictMode>,
//   document.getElementById('root')
// );
