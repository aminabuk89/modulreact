import React, { createContext, useContext, useState, useEffect } from 'react';

const FavoriteContext = createContext();

export const FavoriteProvider = ({ children }) => {
  const [checkFavorite, setCheckFavorite] = useState([]);

  useEffect(() => {
    const storedFavorites = localStorage.getItem("favoriteCard");
    if (storedFavorites) {
      setCheckFavorite(JSON.parse(storedFavorites));
    }
  }, []);

  const setFavoriteCard = (item) => {
    const newFavorite = [...checkFavorite, item];
    setCheckFavorite(newFavorite);
    localStorage.setItem("favoriteCard", JSON.stringify(newFavorite));
  };

  const deleteFavoriteCard = (id) => {
    const newFavorite = checkFavorite.filter((item) => item.id !== id);
    setCheckFavorite(newFavorite);
    localStorage.setItem("favoriteCard", JSON.stringify(newFavorite));
  };

  return (
    <FavoriteContext.Provider value={{ checkFavorite, setFavoriteCard, deleteFavoriteCard }}>
      {children}
    </FavoriteContext.Provider>
  );
};

export const useFavorite = () => useContext(FavoriteContext);