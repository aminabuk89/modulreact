import React, { createContext, useContext, useState, useEffect } from 'react';

const BasketContext = createContext();

export const BasketProvider = ({ children }) => {
  const [checkBasket, setCheckBasket] = useState([]);

  useEffect(() => {
    const storedBasket = localStorage.getItem("basketCard");
    if (storedBasket) {
      setCheckBasket(JSON.parse(storedBasket));
    }
  }, []);

  const setBasketCard = (item) => {
    const newBasket = [...checkBasket, item];
    setCheckBasket(newBasket);
    localStorage.setItem("basketCard", JSON.stringify(newBasket));
  };

  const deleteBasketCard = (id) => {
    const newBasket = checkBasket.filter((item) => item.id !== id);
    setCheckBasket(newBasket);
    localStorage.setItem("basketCard", JSON.stringify(newBasket));
  };

  const clearBasket = () => {
    setCheckBasket([]);
    localStorage.removeItem("basketCard");
  };

  return (
    <BasketContext.Provider value={{ checkBasket, setBasketCard, deleteBasketCard, clearBasket }}>
      {children}
    </BasketContext.Provider>
  );
};

export const useBasket = () => useContext(BasketContext);




// import React, { createContext, useContext, useState } from 'react';

// const BasketContext = createContext();

// export const useBasket = () => useContext(BasketContext);

// export const BasketProvider = ({ children }) => {
//     const [basketItems, setBasketItems] = useState([]);

//     const setBasketCard = (item) => {
//         setBasketItems((prevItems) => [...prevItems, item]);
//     };

//     const deleteBasketCard = (id) => {
//         setBasketItems((prevItems) => prevItems.filter(item => item.id !== id));
//     };

//     return (
//         <BasketContext.Provider value={{ basketItems, setBasketCard, deleteBasketCard }}>
//             {children}
//         </BasketContext.Provider>
//     );
// };