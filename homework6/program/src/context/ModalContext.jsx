// import { createSlice } from '@reduxjs/toolkit';

// const initialState = {
//   modalOpen: false,
//   selectedProductId: null,
//   modalType: null,
// };

// const modalSlice = createSlice({
//   name: 'modal',
//   initialState,
//   reducers: {
//     openModal: (state, action) => {
//       state.modalOpen = true;
//       state.selectedProductId = action.payload.productId;
//       state.modalType = action.payload.modalType;
//     },
//     closeModal: (state) => {
//       state.modalOpen = false;
//       state.selectedProductId = null;
//       state.modalType = null;
//     },
//   },
// });


// export const getStateModal = (state) => state.modal;
// export const { openModal, closeModal } = modalSlice.actions;
// export default modalSlice.reducer;


import React, { createContext, useContext, useState } from 'react';

// Створення контексту
const ModalContext = createContext();

// Провайдер контексту
export const ModalProvider = ({ children }) => {
  const [modalState, setModalState] = useState({
    modalOpen: false,
    selectedProductId: null,
    modalType: null,
  });

  const openModal = (productId, modalType) => {
    setModalState({
      modalOpen: true,
      selectedProductId: productId,
      modalType,
    });
  };

  const closeModal = () => {
    setModalState({
      modalOpen: false,
      selectedProductId: null,
      modalType: null,
    });
  };

  return (
    <ModalContext.Provider value={{ modalState, openModal, closeModal }}>
      {children}
    </ModalContext.Provider>
  );
};

// Хук для використання контексту
export const useModal = () => useContext(ModalContext);