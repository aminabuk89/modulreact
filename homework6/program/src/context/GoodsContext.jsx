import React, { createContext, useContext, useState, useEffect } from 'react';

// Створення контексту для товарів
const GoodsContext = createContext();

export const GoodsProvider = ({ children }) => {
  const [goods, setGoods] = useState([]);
  const [status, setStatus] = useState('idle');
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchGoods = async () => {
      setStatus('loading');
      try {
        const response = await fetch('../goods.json');
        if (!response.ok) {
          throw new Error('Не вдалося отримати товари.');
        }
        const data = await response.json();
        setGoods(data);
        setStatus('succeeded');
      } catch (error) {
        setStatus('failed');
        setError(error.message);
      }
    };

    fetchGoods();
  }, []);

  return (
    <GoodsContext.Provider value={{ goods, status, error }}>
      {children}
    </GoodsContext.Provider>
  );
};

export const useGoods = () => useContext(GoodsContext);

