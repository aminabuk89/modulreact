import ProductList from '../components/ProductList/ProductList';
import Modal from '../components/Modal/Modal';
import { useGoods } from '../context/GoodsContext';
import { useModal } from '../context/ModalContext';

const Home = () => {
    const { goods, status, error } = useGoods();
    const { modalState, closeModal } = useModal();
    const { modalOpen, selectedProductId } = modalState;

    const handleCloseModal = () => {
        closeModal();
    };

    if (status === 'loading') {
        return <div className="text__desc--nothing">Завантаження...</div>;
    } else if (status === 'failed') {
        return <div>{error}</div>;
    }

    return (
        <>
            <h2 className="subtitle__page subtitle__page--center">Асортимент нашого магазину</h2>
            <ProductList data={goods} />
            
            {modalOpen && (
                <Modal data={goods.find((item) => item.id === selectedProductId)} handleCloseModal={handleCloseModal} />
            )}
        </>
    );
};

export default Home;