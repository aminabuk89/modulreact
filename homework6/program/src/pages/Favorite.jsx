import ProductList from "../components/ProductList/ProductList";
import Modal from "../components/Modal/Modal";
import { useFavorite } from "../context/FavoriteContext";
import { useModal } from '../context/ModalContext';

const Favorite = () => {
    const { favoriteItems} = useFavorite();
    const { modalState, closeModal } = useModal();
    const { modalOpen, selectedProductId } = modalState;

    const handleCloseModal = () => {
        closeModal();
    };

    return (
        <>
            <h2 className="subtitle__page subtitle__page--center">Улюблені товари</h2>
            <ProductList data={favoriteItems} />
            
            {modalOpen && (
                <Modal data={favoriteItems.find((item) => item.id === selectedProductId)} handleCloseModal={handleCloseModal} />
            )}
        </>
    );
};

export default Favorite;