import ProductList from "../components/ProductList/ProductList";
import Modal from "../components/Modal/Modal";
import { useBasket } from "../context/BasketContext";
import SignupForm from "../components/Form/Form";
import { useEffect, useState } from "react";
import { useModal } from '../context/ModalContext';

const Basket = () => {

    const { basketItems } = useBasket();

    const { modalState, closeModal } = useModal();
    const { modalOpen, selectedProductId } = modalState;

    const [showDelivery, setShowDelivery] = useState(false);

    useEffect(() => {
        if (basketItems && basketItems.length > 0) {
            setShowDelivery(true);
        } else {
            setShowDelivery(false);
        }
    }, [basketItems]);

    const handleCloseModal = () => {
        closeModal();
    };

    return (
        <>
            <div className={showDelivery ? "main__wrapper" : "main__container"}>
                <div className="flex__element">
                    <h2 className={showDelivery ? "subtitle__page" : "subtitle__page subtitle__page--center"}>Товари з корзини</h2>
                    <ProductList data={basketItems}/>
                </div>
                {showDelivery && <SignupForm />}
            </div>
            
            {modalOpen && (
                <Modal data={basketItems.find((item) => item.id === selectedProductId)} handleCloseModal={handleCloseModal} />
            )}
        </>
    );
};

export default Basket;