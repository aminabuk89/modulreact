import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModalFooter from "./ModalFooter";
import ModalBody from "./ModalBody";
import ModalOut from "../Modal/ModalOut"
import "./Modal.scss"
import { useModal } from '../../context/ModalContext';


const Modal = ({ data }) => {

    const { modalState, closeModal } = useModal();
    const { modalType } = modalState;
    

    const handleCloseModal = () => {
        closeModal();
    };
    
    return (
         <ModalOut handleCloseModal={handleCloseModal}>
            <ModalWrapper title={data.title}>
                <ModalHeader img={data.img} description={data.description}/>
                <ModalBody>
                        {modalType === "basket" &&(
                        <>
                            <ModalFooter
                                firstText="Додати товар до кошика"
                                className="btn__card btn__card--first"
                                handleCloseModal={handleCloseModal}
                            />
                            <ModalFooter
                                secondaryText="Не додавати"
                                className="btn__card btn__card--second"
                                handleCloseModal={handleCloseModal}
                            />
                        </>
                          )} 
                        {modalType === "favorite" && (
                        <>
                            <ModalFooter
                                className="btn__card btn__card--first"
                                text="Додати товар в улюблене"
                                handleCloseModal={handleCloseModal}
                            />
                            <ModalFooter
                                secondaryText="Не додавати"
                                className="btn__card btn__card--second"
                                handleCloseModal={handleCloseModal}
                            />
                        </>
                        )}
                        {modalType === "basketClose" && (
                        <>
                            <ModalFooter
                                className="btn__card btn__card--first"
                                textBasketClose="Видалити товар з корзини"
                                handleCloseModal={handleCloseModal}
                            />
                            <ModalFooter
                                saveGoods="Залишити товар"
                                className="btn__card btn__card--second"
                                handleCloseModal={handleCloseModal}
                            />
                        </>
                        )}
                        {modalType === "favoriteClose" && (
                        <>
                            <ModalFooter
                                className="btn__card btn__card--first"
                                textFavoriteClose="Видалити товар з улюбленого"
                                handleCloseModal={handleCloseModal}
                            />
                            <ModalFooter
                                saveGoods="Залишити товар"
                                className="btn__card btn__card--second"
                                handleCloseModal={handleCloseModal}
                            />
                        </>
                        )}
                    
                </ModalBody>
            </ModalWrapper>
        </ModalOut>
    );
}

export default Modal;


