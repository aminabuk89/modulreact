import { useModal } from '../../context/ModalContext';
import { useGoods } from '../../context/GoodsContext';
import { useBasket} from '../../context/BasketContext';
import { useFavorite } from '../../context/FavoriteContext';
// import { deleteBasketCard } from "../../context/BasketContext";
const ModalFooter = ({ firstText, className, secondaryText,  text, textBasketClose, textFavoriteClose, saveGoods, handleCloseModal }) => {

  const { modalState } = useModal();
  const { selectedProductId } = modalState;

  const { goods } = useGoods();
  const { setBasketCard, deleteBasketCard } = useBasket(); // Використання з useBasket без окремого імпорту deleteBasketCard
  const { setFavoriteCard, deleteFavoriteCard } = useFavorite();

      const addToCardBasket = () => {
        const data = goods.find(item => item.id === selectedProductId)
        if (data) {
            setBasketCard(data)
        }

        handleCloseModal();      
      };

      const addToCardFavorite = () => {
        const data = goods.find(item => item.id === selectedProductId);

        if (data) {
            setFavoriteCard(data);
        };

        handleCloseModal();      
      };

      const deleteCardBasket = () => {
        deleteBasketCard(selectedProductId)
        handleCloseModal();
      }

      const deleteCardFavorite = () => {
        deleteFavoriteCard(selectedProductId)
        handleCloseModal();
      }
  
      
  return (

    <div className="modal__footer">

      {firstText &&  (
        <button onClick={addToCardBasket} className={className}>{firstText}</button>
      )}

      {text && (
        <button onClick={addToCardFavorite} className={className}>{text}</button>
      )}

      {secondaryText &&  (
        <button onClick={handleCloseModal} className={className}>{secondaryText}</button>
      )}
      {textBasketClose && (
        <button onClick={deleteCardBasket} className={className}>{textBasketClose}</button>
      )}
      {textFavoriteClose && (
        <button onClick={deleteCardFavorite} className={className}>{textFavoriteClose}</button>
      )}
      {saveGoods &&  (
        <button onClick={handleCloseModal} className={className}>{saveGoods}</button>
      )}

   </div>
  );
};

export default ModalFooter;

