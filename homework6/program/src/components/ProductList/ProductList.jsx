import Card from "../Card/Card";

const ProductList = ({data}) => {

    if (!data || data.length === 0) {
        return <p className="text__desc--nothing">Збережених товарів немає</p>;
    }


    

    return (
        <ul className="card__list">
            {data.map((product, index) => {
                return (<Card
                    key={index}
                    id={product.id}
                    title={product.title}
                    img={product.img}
                    color={product.color}
                    price={product.price}
                    article={product.article}
                    
                />)
            }
            )}
        </ul>
        )
    
}
export default ProductList;
