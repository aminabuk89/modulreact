const Footer = () => {
    return (
        <footer className="footer__page">
            <p className="footer__page--text">Copyright © 2023 Euphoria Folks Pvt Ltd. All rights reserved.</p>
        </footer>
    )
}
export default Footer;