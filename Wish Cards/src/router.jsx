import { createBrowserRouter } from 'react-router-dom';
import Layout from './Layout';
import Home from './pages/Home';
import Stickers from "./pages/Stickers";
import Cards from './pages/Cards';


const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        index: true,
        element: <Home />
      },
      {
        path: "/cards",
        element: <Cards />
      },
      {
        path: "/stickers",
        element: <Stickers />
      }
    ]
  }

])

export default router;