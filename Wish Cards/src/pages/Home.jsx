import predictions from "../assets/predictions.webp";

const Home = () => {
    return (
        <main className="main__page">
            <h2 className="subtitle__page">Карти з прогнозами</h2>
            <ul className="card__list">
                <li className="card__item">
                    <img className="card__img" src={predictions} alt="card predictions" />
                </li>
                <li className="card__item">
                    <img className="card__img" src={predictions} alt="card predictions" />
                </li>
                <li className="card__item">
                    <img className="card__img" src={predictions} alt="card predictions" />
                </li>
                <li className="card__item">
                    <img className="card__img" src={predictions} alt="card predictions" />
                </li>
                <li className="card__item">
                    <img className="card__img" src={predictions} alt="card predictions" />
                </li>
                <li className="card__item">
                    <img className="card__img" src={predictions} alt="card predictions" />
                </li>
                <li className="card__item">
                    <img className="card__img" src={predictions} alt="card predictions" />
                </li>
                <li className="card__item">
                    <img className="card__img" src={predictions} alt="card predictions" />
                </li>
                <li className="card__item">
                    <img className="card__img" src={predictions} alt="card predictions" />
                </li>
                <li className="card__item">
                    <img className="card__img" src={predictions} alt="card predictions" />
                </li>
                <li className="card__item">
                    <img className="card__img" src={predictions} alt="card predictions" />
                </li>
            </ul>
        </main>
    
    )
}
export default Home;