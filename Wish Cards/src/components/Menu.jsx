import { Link } from "react-router-dom";

const Menu = () => {
    
    return (
        <ul className="menu__list">
            <li className="menu__item">
                <Link to="/" className="menu__link">Головна сторінка</Link>
            </li>
            <li className="menu__item">
                <Link to="/cards" className="menu__link">Карти бажань</Link>
            </li>
            <li className="menu__item">
                <Link to="/stickers" className="menu__link">Стікери</Link>
            </li>
        </ul>
    )
}

export default Menu;