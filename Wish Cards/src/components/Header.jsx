import Menu from "./Menu";

const Header = () => {
    return (
        <header className="header__page">
            <h1 className="main__title">tut.is.zaraz</h1>
            <Menu/> 
        </header>
    )
}
export default Header;