import './index.scss'
import Header from './components/Header';
import { Outlet } from 'react-router-dom';

const Layout = () => {
  
  return (
    <div className='wrapper'>
        <div className='container'>
          <Header />
          <Outlet />
        </div>
    </div>
  )
}
export default Layout;
